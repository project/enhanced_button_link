<?php

namespace Drupal\enhanced_button_link;

/**
 * Defines an exception thrown when parsing operations fail.
 */
class EnhancedButtonLinkParseException extends \Exception {}
